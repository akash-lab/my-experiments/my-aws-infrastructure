terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "s3" {
    bucket = "my-teraform-state-files"
    key = "v1/dev/terraform-lambda"
    region = "us-east-1"
    profile = "lambda"
  }
}

provider "aws" {
  region = "us-east-1"
  profile = "lambda"
}


module "module_network" {
  #  source  = "git@github.com:your-organization/the-repository-name.git?ref=1.0.0"
  source = "../modules/networks"
  env = "stage"
  vpc_name = "akt-vpc"
  subnet_name = "akt-subnet"
  secgrp_name = "akt-secgrp"
}

resource "aws_iam_role" "example" {
  name = "example-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "lambda.amazonaws.com"
      }
    }]
  })
}

resource "aws_iam_policy" "example" {
  name        = "example-policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect = "Allow"
      Action = [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
      Resource = ["arn:aws:logs:*:*:*"]
    },{
      Effect = "Allow"
      Action = [
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface"
      ]
      Resource = ["*"]
    }]
  })
}

resource "aws_iam_role_policy_attachment" "example" {
  policy_arn = aws_iam_policy.example.arn
  role = aws_iam_role.example.name
}

resource "aws_lambda_function" "example_fnc" {
  function_name    = "example-lambda"
  filename         = "${path.module}/python/app.zip"
  source_code_hash = filebase64sha256("${path.module}/python/app.zip")
  handler          = "app.lambda_handler"
  role             = aws_iam_role.example.arn
  runtime          = "python3.8"
  vpc_config {
    subnet_ids = [module.module_network.return_subnet_id]
    security_group_ids = [module.module_network.return_secgrp_id]
  }
}

resource "aws_lambda_function_url" "lambda_function_url" {
  function_name      = aws_lambda_function.example_fnc.arn
  authorization_type = "NONE"
}

output "function_url" {
  description = "Function URL."
  value       = aws_lambda_function_url.lambda_function_url.function_url
}

# data "archive_file" "zip_the_python_code" {
#     type        = "zip"
#     source_file  = "${path.module}/python/app.py"
#     output_path = "${path.module}/python/app.zip"
# }