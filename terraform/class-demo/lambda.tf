

# VPC # Subnets # Sec grps
module "module_network" {
    source="./module/network"
    env="uat"
}

# lambda function
resource "aws_iam_role" "example" {
  name = "demo-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "lambda.amazonaws.com"
      }
    }]
  })
}

resource "aws_iam_policy" "example" {
  name        = "demo-policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect = "Allow"
      Action = [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
      Resource = ["arn:aws:logs:*:*:*"]
    },{
      Effect = "Allow"
      Action = [
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface"
      ]
      Resource = ["*"]
    }]
  })
}

resource "aws_iam_role_policy_attachment" "example" {
  policy_arn = aws_iam_policy.example.arn
  role = aws_iam_role.example.name
}

resource "aws_lambda_function" "example_fnc" {
  function_name    = "demo-lambda"
  filename         = "${path.module}/python/app.zip"
  source_code_hash = filebase64sha256("${path.module}/python/app.zip")
  handler          = "app.lambda_handler"
  role             = aws_iam_role.example.arn
  runtime          = "python3.8"
  vpc_config {
    subnet_ids = [module.module_network.return_subnet_id]
    security_group_ids = [module.module_network.return_secgrp_id]
  }
}



