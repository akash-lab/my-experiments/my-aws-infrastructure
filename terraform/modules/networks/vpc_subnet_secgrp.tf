
# Variables for VPC, Subnet, and Security Group
variable "env" {
  type = string
  default = "env-dev"
}

variable "vpc_name" {
  type = string
  default = "my-vpc"
}

variable "subnet_name" {
  type = string
  default = "my-subnet"
}

variable "secgrp_name" {
  type = string
  default = "my-secgrp"
}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "${var.env}-${var.vpc_name}"
  }
}

resource "aws_subnet" "subnet" {
  cidr_block = "10.0.1.0/24"
  vpc_id = aws_vpc.vpc.id
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true
  
  tags = {
    Name = "${var.env}-${var.vpc_name}-${var.subnet_name}"
  }
}

resource "aws_security_group" "security_group" {
  name_prefix = "${var.env}-${var.vpc_name}-${var.subnet_name}-${var.secgrp_name}"
  vpc_id = aws_vpc.vpc.id

#   ingress {
#     from_port = 0
#     to_port = 65535
#     protocol = "tcp"
#     cidr_blocks = ["10.0.1.0/24"]
#   }

  ingress {
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "return_vpc_id" {
 value       = aws_vpc.vpc.id
 description = "VPC Id returned"
}

output "return_subnet_id" {
 value       = aws_subnet.subnet.id
 description = "Subnet Id returned"
}

output "return_secgrp_id" {
 value       = aws_security_group.security_group.id
 description = "Sec Grp Id returned"
}