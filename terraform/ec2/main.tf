variable "awsprops" {
    default = {
        region = "us-east-1"
        vpc = "vpc-511de02c" # change this to your vpc id
        ami = "ami-051f7e7f6c2f40dc1"
        itype = "t2.micro"
        subnet = "subnet-e3f05dbc" # change this to your subnet id
        publicip = true
        keyname = "my-static-site-key-pair" #change this to your key name
        secgroupname = "tf-my-ec2-sec-grp"
  }
}

provider "aws" {
  region = lookup(var.awsprops, "region")
}

resource "aws_security_group" "project-iac-sg" {
  name = lookup(var.awsprops, "secgroupname")
  description = lookup(var.awsprops, "secgroupname")
  vpc_id = lookup(var.awsprops, "vpc")

  // To Allow SSH Transport
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 80 Transport
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "project-iac" {
  ami = lookup(var.awsprops, "ami")
  instance_type = lookup(var.awsprops, "itype")
  subnet_id = lookup(var.awsprops, "subnet") #FFXsubnet2
  associate_public_ip_address = lookup(var.awsprops, "publicip")
  key_name = lookup(var.awsprops, "keyname")


  vpc_security_group_ids = [
    aws_security_group.project-iac-sg.id
  ]
#   root_block_device {
#     delete_on_termination = true
#     iops = 150
#     volume_size = 50
#     volume_type = "gp2"
#   }
  tags = {
    Name ="tf-my-ec2-instance"
    Environment = "DEV"
    OS = "UBUNTU"
    Managed = "IAC"
  }

  depends_on = [ aws_security_group.project-iac-sg ]

  user_data = <<-EOF
        #!/bin/bash
        echo "Hello from user data script!" > /tmp/user_data_output.txt

        # Update the system
        sudo yum update -y

        # Install Docker dependencies
        sudo yum install -y docker


        # Start the Docker service
        sudo service docker start

        # Add the user to the docker group (optional, allows running Docker commands without sudo)
        sudo usermod -aG docker ec2-user

        # Enable Docker to start on boot
        sudo chkconfig docker on

        sudo yum install nginx -y

        sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak

        sudo sed -i '/^#/d' /etc/nginx/nginx.conf
        sudo sed -i '/error_page 404/i\
        \
        location /flask {\
            proxy_pass http://localhost:8085/;\
        }\
        \
        ' /etc/nginx/nginx.conf

        sudo sed -i '/error_page 404/i\
        \
        location /appointments {\
            proxy_pass http://localhost:8081/;\
        }\
        \
        ' /etc/nginx/nginx.conf

                sudo sed -i '/error_page 404/i\
        \
        location /doctor {\
            proxy_pass http://localhost:8082/;\
        }\
        \
        ' /etc/nginx/nginx.conf

        sudo sed -i '/error_page 404/i\
        \
        location /patient {\
            proxy_pass http://localhost:8083/;\
        }\
        \
        ' /etc/nginx/nginx.conf

        sudo sed -i '/error_page 404/i\
        \
        location /frontend {\
            proxy_pass http://localhost:8084/;\
        }\
        \
        ' /etc/nginx/nginx.conf

        
        sudo systemctl start nginx
        sudo systemctl enable nginx

        # Install Kosli - delete this later just for trial
        # Others please ignore this
        curl -L https://github.com/kosli-dev/cli/releases/download/v2.6.5/kosli_2.6.5_linux_amd64.tar.gz | tar zx
        mv kosli /usr/local/bin/kosli

        EOF

}


output "ec2instance" {
  value = aws_instance.project-iac.public_ip
}

# output "availability_zone" {
#   value = aws_instance.project-iac.availability_zone
# }