resource "aws_lb_target_group" "my_target_grp" {
  name       = "my-app-eg1"
  port       = 8080
  protocol   = "HTTP"
  vpc_id     = module.my_network.return_vpc_id
  slow_start = 0

  load_balancing_algorithm_type = "round_robin"

  stickiness {
    enabled = false
    type    = "lb_cookie"
  }

  health_check {
    enabled             = true
    port                = 8081
    interval            = 30
    protocol            = "HTTP"
    path                = "/health"
    matcher             = "200"
    healthy_threshold   = 3
    unhealthy_threshold = 3
  }
}

resource "aws_lb_target_group_attachment" "attach_ec2_to_tg" {
  count=3
  target_group_arn = aws_lb_target_group.my_target_grp.arn
  target_id        = aws_instance.ec2-instances[count.index].id
  port             = 8080
}
