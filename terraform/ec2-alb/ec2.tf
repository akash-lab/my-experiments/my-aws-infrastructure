data "aws_ami" "random_ami" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

resource "aws_instance" "ec2-instances" {
  count                       = 3
  ami                         = data.aws_ami.random_ami.id
  instance_type               = "t2.micro"
  subnet_id                   = module.my_network.return_subnet_id
  associate_public_ip_address = true
  key_name                    = "my-key-pair"

  vpc_security_group_ids = [
    module.my_network.return_secgrp_id
  ]

  tags = {
    Name = "akt-ec2-instance-${count.index}"
  }

}