
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

module "my_network" {
  source      = "../modules/networks"
  env         = "dev"
  vpc_name    = "tf-alb-vpc"
  subnet_name = "tf-alb-subnet"
  secgrp_name = "tf-alb-secgrp"
}

